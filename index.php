<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Petits chevaux</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/login.js"></script>

  </head>
  <body>

    <div id="banniereConnexion">
      <div id="connexion">
        <h1>Les petits chevaux : The video game</h1>
        <h1>Connexion</h1>
        <form>
          <label>Login : </label><input type="text" name="login" id="login">
          <span id="log">Valider</span>
        </form>
      </div>
    </div>
    <div id="parties"></div>
  </body>
</html>
