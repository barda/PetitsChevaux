<?php
class partieController{
  protected $db;

  public function __construct($db){
    $this->db=$db;
  }

  public function getPartiesDispo(){
    $listParties=array();
    $sql="SELECT * FROM partie WHERE nbJoueurs < 4";
    $req = $this->db->prepare($sql);
    $req->execute();
    while($line = $req->fetch(PDO::FETCH_OBJ)){
      $listParties[] = new partie($line);
    }
    return $listParties;
  }

  public function addNewGame($nom, $nb, $aCommence, $createur, $enjeu){
    $sql="INSERT INTO partie(nomPartie, nbJoueurs, aCommence, idJoueurCreateur, idJoueurEnJeu) VALUES (:nom, :nb, :aCommence, :createur, :enjeu)";
    $req = $this->db->prepare($sql);
    $req->bindValue(":nom", $nom, PDO::PARAM_STR);
    $req->bindValue(":nb", $nb, PDO::PARAM_INT);
    $req->bindValue(":aCommence", $aCommence, PDO::PARAM_INT);
    $req->bindValue(":createur", $createur, PDO::PARAM_INT);
    $req->bindValue(":enjeu", $enjeu, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  public function addPlayerInPartie($idPartie, $nbJoueurs){
    $sql="UPDATE partie SET nbJoueurs=:nbJoueurs WHERE idPartie=:id";
    $req = $this->db->prepare($sql);
    $req->bindValue(":id", $idPartie, PDO::PARAM_INT);
    $req->bindValue(":nbJoueurs", $nbJoueurs, PDO::PARAM_INT);
    $req->execute();
  }

  public function getNbJoueurs($idPartie){
      $sql = "SELECT nbJoueurs FROM partie WHERE idPartie = :idPartie";
      $req = $this->db->prepare($sql);
      $req->bindValue(":idPartie", $idPartie, PDO::PARAM_INT);
      $req->execute();
      return $req->fetch(PDO::FETCH_ASSOC)['nbJoueurs'];
  }
}
 ?>
