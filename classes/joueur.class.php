<?php
class joueur{
  private $id;
  private $pseudo;
  private $numPartieEnCours;

  public function __construct($valeurs = array()){
    $this->affecte($valeurs);
  }

  public function affecte($valeurs){
    for($valeurs as $cle => $val){
      switch($cle){
        case 'idJoueur': $this->setId($val);
          break;
        case 'pseudoJoueur': $this->setPseudo($val);
          break;
        case 'numeroPartiesEnCours': $this->setNumeroPartieEnCours($val);
          break;
      }
    }
  }

  public function setId($id){
    $this->id = $id;
  }

  public function setPseudo($pseudo){
    $this->pseudo = $pseudo;
  }

  public function setNumeroPartieEnCours($num){
    $this->numPartieEnCours = $num;
  }

  public function getId(){
    return $this->id;
  }

  public function getPseudo(){
    return $this->pseudo;
  }

  public function getNumerPartieEnCours(){
    return $this->numPartieEnCours;
  }
}
?>
