<?php
class DeroulementPartieController{
  protected $db;

  public function __construct($db){
    $this->db=$db;
  }

  public function addJoueurInDeroulementPartie($partie, $joueur){
    $sql="INSERT INTO deroulementPartie (idJoueur, idPartie, cheval1, cheval2, cheval3, cheval4) VALUES (:idJoueur, :idPartie, 0, 0, 0, 0)";
    $req = $this->db->prepare($sql);
    $req->bindValue(":idJoueur", $joueur, PDO::PARAM_INT);
    $req->bindValue(":idPartie", $partie, PDO::PARAM_INT);
    $req->execute();
  }
}
?>
