<?php
class DéroulementPartie{
  private $idPartie;
  private $idJoueur;
  private $cheval1;
  private $cheval2;
  private $cheval3;
  private $cheval4;

  public function __construct($valeurs = array()){
    $this->affecte($valeurs);
  }

  public function affecte($valeurs){
    for($valeurs as $cle => $val){
      switch($cle){
        case 'idPartie': $this->setIdPartie($val);
          break;
        case 'idJoueur': $this->setIdJoueur($val);
          break;
        case 'cheval1': $this->setCheval1($val);
          break;
        case 'cheval2': $this->setCheval2($val);
          break;
        case 'cheval3': $this->setCheval3($val);
          break;
        case 'cheval4': $this->setCheval4($val);
          break;
      }
    }
  }

  public function setIdPartie($val){
    $this->idPartie=$val;
  }

  public function setIdJoueur($val){
    $this->idJoueur=$val;
  }

  public function setCheval1($val){
    $this->cheval1=$val;
  }

  public function setCheval2($val){
    $this->cheval2=$val;
  }

  public function setCheval3($val){
    $this->cheval3=$val;
  }

  public function setCheval4($val){
    $this->cheval4=$val;
  }

  public function getIdPartie(){
    return $this->idPartie;
  }

  public function getIdJoueur(){
    return $this->idJoueur;
  }

  public function getCheval1(){
    return $this->cheval1;
  }

  public function getCheval2(){
    return $this->cheval2;
  }

  public function getCheval3(){
    return $this->cheval3;
  }

  public function getCheval4(){
    return $this->cheval4;
  }
}
?>
