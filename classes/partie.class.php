<?php
class partie{
  private $idPartie;
  private $nomPartie;
  private $nbJoueurs;
  private $aCommence;

  public function __construct($valeurs = array()){
    $this->affecte($valeurs);
  }

  public function affecte($valeurs){
    foreach ($valeurs as $cle => $val) {
      switch($cle){
        case 'idPartie': $this->setId($val);
          break;
        case 'nomPartie': $this->setNom($val);
          break;
        case 'nbJoueurs': $this->setNbJoueurs($val);
          break;
        case 'aCommence': $this->setACommence($val);
          break;
      }
    }
  }

  public function setId($id){
    $this->idPartie=$id;
  }

  public function setNom($nom){
    $this->nomPartie=$nom;
  }

  public function setNbJoueurs($nbJoueurs){
    $this->nbJoueurs=$nbJoueurs;
  }

  public function setACommence($bool){
    $this->aCommence = $bool;
  }

  public function getId(){
    return $this->idPartie;
  }

  public function getNom(){
    return $this->nomPartie;
  }

  public function getNbJoueurs(){
    return $this->nbJoueurs;
  }

  public function getACommence(){
    return $this->aCommence;
  }
}
 ?>
