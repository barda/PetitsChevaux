<?php
class joueurController{
  protected $db;

  public function __construct($db){
    $this->db = $db;
  }

  public function existJoueur($login){
    $sql="SELECT pseudoJoueur FROM joueur WHERE pseudoJoueur=:pseudo";
    $req = $this->db->prepare($sql);
    $req->bindValue(":pseudo", $login, PDO::PARAM_STR);
    $req->execute();
    $result = $req->fetch(PDO::FETCH_ASSOC);
    if($result){
      return true;
    }else{
      return false;
    }
  }

  public function ajoutJoueur($login){
    $sql="INSERT INTO joueur (pseudoJoueur) VALUES (:pseudo)";
    $req = $this->db->prepare($sql);
    $req->bindValue(":pseudo", $login, PDO::PARAM_STR);
    $req->execute();
  }

  public function getPlayer($nom){
    $sql="SELECT idJoueur FROM joueur WHERE pseudoJoueur=:nom";
    $req = $this->db->prepare($sql);
    $req->bindValue(":nom", $nom, PDO::PARAM_STR);
    $req->execute();
    echo $req->fetch(PDO::FETCH_ASSOC)['idJoueur'];
    return $req->fetch(PDO::FETCH_ASSOC)['idJoueur'];
  }
}
 ?>
