
  <header>
    <?php
      require("./config/config.inc.php");
      require("./config/autoload.inc.php");
     ?>
  </header>
  <div id="pageListerParties">
    <?php
      $db = new Mypdo();
      $joueur = new joueurController($db);
     ?>
    <script>
      console.log(<?php $_POST["login"] ?>);
      $.cookie("idJoueur", <?php $joueur->getPlayer($_POST["login"]); ?>);
      console.log(document.cookie);
      $.cookie("user", <?php echo $_POST["login"]; ?>);
    </script>
    <h1>Les petits chevaux</h1>
    <br>
    <?php
    $db = new Mypdo();
    $joueurCtrl = new joueurController($db);
    $partieCtrl = new partieController($db);
    echo "<p>Bonjour <span id='nomCreateur'>".$_POST["login"]."</span>,</p>\n";
    if(!$joueurCtrl->existJoueur($_POST["login"])){
      $joueurCtrl->ajoutJoueur($_POST["login"]);
      echo "<p>Vous avez été enregistré dans la base de données</p>";
    }

    $listParties = $partieCtrl->getPartiesDispo();
    ?>
    <h2>Liste des parties disponibles</h2>
    <p><a href="/PetitsChevaux/plateau.php">Vous n'avez pas de curly ? N'hesitez pas à commencer une partie solo</a></p>
    <table id="ListePartiesDispo">
      <tr>
        <th>Nom de la partie</th>
        <th class="separateur">Nombre de joueurs</th>
        <th class="separateur">Rejoindre la partie</th>
      </tr>
      <?php
      foreach ($listParties as $partie) {
        echo "<tr>";
        echo "<td>".$partie->getNom()."</td>";
        echo "<td class='separateur'>".$partie->getNbJoueurs()."</td>";
        echo "<td class='separateur'><a href='/PetitsChevaux/plateauMulti.php?idPartie=".$partie->getId()."&nbJoueurs=".$partie->getNbJoueurs()."'>Rejoindre la partie</a></td>";
        echo "</tr>";
      }
        ?>
    </table>
    <h2>Créer une partie</h2>
    <form>
      <fieldset>
        <legend>Commencer une partie multi-joueur</legend>
        <label>Nom de la partie : </label><input type="text" name="nomPartie" id="nomPartie">
        <span id="nouvellePartie">Envoyer</span>
      </fieldset>
    </form>
   </div>
   <script type="text/javascript" src="js/partie.js"></script>
