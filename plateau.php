<head>
  <meta charset="utf-8">
  <title>Petits chevaux</title>
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/plateau.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="js/partie.js"></script>
  <script src="js/de.js"></script>
</head>
  <header>
    <?php
      require("./config/config.inc.php");
      require("./config/autoload.inc.php");
     ?>
  </header>
  <?php
    $db = new Mypdo();
    $partieCtrl = new partieController($db);
    if((!empty($_GET["idPartie"]))&&(!empty($_GET["nbJoueurs"]))){
      $nb = $_GET["nbJoueurs"]+1;
      $partieCtrl->addPlayerInPartie($_GET["idPartie"], $nb);
    }
    if(!empty($_POST["nomPartie"])){
      $partieCtrl->addNewGame($_POST["nomPartie"], 1, 0, $_POST["nomCreateur"]);
    }
   ?>
   <div id="content">
    <div id="plateau">
      <div id="ecurieBleue" class="ecurie">
        <img src="./images/pions/ChevalBleu.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalBleu.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalBleu.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalBleu.png" alt="cheval bleu" width="34px" height="34px" class="pion">
      </div>

      <div id="colonne1" class="colonne">
        <div class="rond bleu" id="rb12"></div>
        <div class="rond bleu" id="rb11"></div>
        <div class="rond bleu" id="rb10"></div>
        <div class="rond bleu" id="rb9"></div>
        <div class="rond bleu" id="rb8"></div>
        <div class="rond bleu" id="rb7"></div>
      </div>

      <div id="colonne2" class="colonne">
        <div class="rond bleu" id="rb13"></div>
        <div class="carre rouge" id="cr1"></div>
        <div class="carre rouge" id="cr2"></div>
        <div class="carre rouge" id="cr3"></div>
        <div class="carre rouge" id="cr4"></div>
        <div class="carre rouge" id="cr5"></div>
      </div>

      <div id="colonne3" class="colonne">
        <div class="rond rouge" id="rr0"></div>
        <div class="rond rouge" id="rr1"></div>
        <div class="rond rouge" id="rr2"></div>
        <div class="rond rouge" id="rr3"></div>
        <div class="rond rouge" id="rr4"></div>
        <div class="rond rouge" id="rr5"></div>
      </div>

      <div id="ecurieRouge" class="ecurie">
        <img src="./images/pions/ChevalRouge.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalRouge.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalRouge.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalRouge.png" alt="cheval bleu" width="34px" height="34px" class="pion"></div>

      <br />

      <div id="ligne1" class="ligne">
        <div class="rond bleu" id="rb0"></div>
        <div class="rond bleu" id="rb1"></div>
        <div class="rond bleu" id="rb2"></div>
        <div class="rond bleu" id="rb3"></div>
        <div class="rond bleu" id="rb4"></div>
        <div class="rond bleu" id="rb5"></div>
        <div class="rond bleu" id="rb6"></div>
        <div class="carre rouge" id="cr6"></div>
        <div class="rond rouge" id="rr6"></div>
        <div class="rond rouge" id="rr7"></div>
        <div class="rond rouge" id="rr8"></div>
        <div class="rond rouge" id="rr9"></div>
        <div class="rond rouge" id="rr10"></div>
        <div class="rond rouge" id="rr11"></div>
        <div class="rond rouge" id="rr12"></div>
      </div>

      <br />

      <div id="ligne2" class="ligne">
        <div class="rond vert" id="rv13"></div>
        <div class="carre bleu" id="cb1"></div>
        <div class="carre bleu" id="cb2"></div>
        <div class="carre bleu" id="cb3"></div>
        <div class="carre bleu" id="cb4"></div>
        <div class="carre bleu" id="cb5"></div>
        <div class="carre bleu" id="cb6"></div>
        <div class="carre invisible"></div>
        <div class="carre jaune" id="cj6"></div>
        <div class="carre jaune" id="cj5"></div>
        <div class="carre jaune" id="cj4"></div>
        <div class="carre jaune" id="cj3"></div>
        <div class="carre jaune" id="cj2"></div>
        <div class="carre jaune" id="cj1"></div>
        <div class="rond rouge" id="rr13"></div>
      </div>

      <br />

      <div id="ligne3" class="ligne">
        <div class="rond vert" id="rv12"></div>
        <div class="rond vert" id="rv11"></div>
        <div class="rond vert" id="rv10"></div>
        <div class="rond vert" id="rv9"></div>
        <div class="rond vert" id="rv8"></div>
        <div class="rond vert" id="rv7"></div>
        <div class="rond vert" id="rv6"></div>
        <div class="carre vert" id="cv6"></div>
        <div class="rond jaune" id="rj6"></div>
        <div class="rond jaune" id="rj5"></div>
        <div class="rond jaune" id="rj4"></div>
        <div class="rond jaune" id="rj3"></div>
        <div class="rond jaune" id="rj2"></div>
        <div class="rond jaune" id="rj1"></div>
        <div class="rond jaune" id="rj0"></div>
      </div>

      <br />

      <div id="ecurieVerte" class="ecurie">
                <img src="./images/pions/ChevalVert.png" alt="cheval bleu" width="34px" height="34px" class="pion">
                <img src="./images/pions/ChevalVert.png" alt="cheval bleu" width="34px" height="34px" class="pion">
                <img src="./images/pions/ChevalVert.png" alt="cheval bleu" width="34px" height="34px" class="pion">
                <img src="./images/pions/ChevalVert.png" alt="cheval bleu" width="34px" height="34px" class="pion"></div>

      <div id="colonne4" class="colonne">
        <div class="rond vert" id="rv5"></div>
        <div class="rond vert" id="rv4"></div>
        <div class="rond vert" id="rv3"></div>
        <div class="rond vert" id="rv2"></div>
        <div class="rond vert" id="rv1"></div>
        <div class="rond vert" id="rv0"></div>
      </div>

      <div id="colonne5" class="colonne">
        <div class="carre vert" id="cv5"></div>
        <div class="carre vert" id="cv5"></div>
        <div class="carre vert" id="cv5"></div>
        <div class="carre vert" id="cv5"></div>
        <div class="carre vert" id="cv5"></div>
        <div class="rond jaune" id="rj13"></div>
      </div>

      <div id="colonne6" class="colonne">
        <div class="rond jaune" id="rj7"></div>
        <div class="rond jaune" id="rj8"></div>
        <div class="rond jaune" id="rj9"></div>
        <div class="rond jaune" id="rj10"></div>
        <div class="rond jaune" id="rj11"></div>
        <div class="rond jaune" id="rj12"></div>
      </div>

      <div id="ecurieJaune" class="ecurie">
        <img src="./images/pions/ChevalJaune.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalJaune.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalJaune.png" alt="cheval bleu" width="34px" height="34px" class="pion">
        <img src="./images/pions/ChevalJaune.png" alt="cheval bleu" width="34px" height="34px" class="pion"></div>

    </div>

    <div id="PackageDe">
      <p id="roll">Lancer le dé</p>
      <center><img id="imgde"><center>
    </div>
  </div>
